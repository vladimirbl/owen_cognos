<?php
    class View {
        private static $varTemplate = [];

        public static function show(string $file, array $params = [], array $templates = []):void {
            $file = explode('|', $file);

            if(count($file) > 1){
                $content = file_get_contents(DIRECTORY_MODULES . $file[0] . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $file[1] . ".owen");
            } else {
                $content = file_get_contents(DIRECTORY_VIEWS . $file[0] . ".owen");
            }

            $content = self::replaceTemplates($content, $templates);
            $content = self::replaceVariables($content, $params);

            foreach(self::$varTemplate as $key => $val){
                ${$key} = $val;
            }

            function E($text, $mostrar = true){
                if($mostrar) echo \View::escapeString((string) $text);
                else return \View::escapeString((string) $text);
            }
            
            eval(" ?>" . $content);
        }

        public static function escapeString(string $text):string {
            return htmlspecialchars($text);
        }

        private static function replaceVariables(string $html, array $params){

            preg_match_all('/\@\@([A-Za-z_][A-Za-z0-9_]*)(\[[\'\"A-Za-z0-9-_]+\])?/', $html, $variables);

            foreach($variables[1] as $key => $val){
                if(isset($params[$val])){
                    $variables[1][$key] = $params[$val];
                    unset($params[$val]);
                    if(is_string($variables[1][$key])){
                        $variables[1][$key] = self::escapeString($variables[1][$key]);
                    }
                    self::$varTemplate[$val]  = $variables[1][$key];
                } else {
                    $variables[1][$key] = '';
                }
            }

            foreach($params as $key => $val){
                self::$varTemplate[$key] = $val;
            }
            
            return str_replace($variables[0], $variables[1], $html);

        }

        private static function replaceTemplates(string $html, array $templates){
            $templates['sidebar'] = 'main/sidebar.administrator';

            preg_match_all('/\#\#([A-Za-z_][A-Za-z0-9_]*)(\[[\'\"A-Za-z0-9-_]+\])?/', $html, $variables);

            foreach($variables[1] as $key => $val){
                if(isset($templates[$val])){
                    $templates[$val] = explode('|', $templates[$val]);

                    if(count($templates[$val]) > 1)
                        $dir = DIRECTORY_MODULES . $templates[$val][0] . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $templates[$val][1] . ".owen";
                    else
                        $dir = DIRECTORY_VIEWS . $templates[$val][0] . ".owen";

                    if(is_file($dir)){
                        $content = file_get_contents($dir);
                        // Include modules sidebar
                        if($templates[$val][0] == "main/dashboard") $content .= \Modules::getDashboard();
                        if($val == 'sidebar') $content .= \Modules::getSidebar();
                        // END include
                        $variables[1][$key] = $content;
                    }
                } else {
                    $variables[1][$key] = '';
                }
            }
            
            return str_replace($variables[0], $variables[1], $html);

        }
    }

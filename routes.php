<?php

    Routes::get('/', 'Authentication/Authentication.index');

    Routes::post('/', 'Authentication/Authentication.checkAuthentication');

    Routes::get('/main', 'Main/Main.index', true);
    Routes::get('/logout', 'Authentication/Authentication.logout', true);

    Routes::get('/admin/usuarios', 'Management/Users.index', true, ['ADMIN']);
    Routes::get('/admin/usuarios/nuevo', 'Management/Users.createForm', true, ['ADMIN']);
    Routes::post('/admin/usuarios/nuevo', 'Management/Users.create', true, ['ADMIN']);
    Routes::post('/admin/usuarios/editar', 'Management/Users.edit', true, ['ADMIN']);
    Routes::post('/admin/usuarios/eliminar', 'Management/Users.delete', true, ['ADMIN']);

    Routes::get('/admin/modulos', 'Management/Modules.index', true, ['ADMIN']);
    Routes::post('/admin/modulos', 'Management/Modules.changeStatus', true, ['ADMIN']);

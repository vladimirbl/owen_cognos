<?php
    define("DIRECTORY_BASE", __DIR__ . DIRECTORY_SEPARATOR);
    define("DIRECTORY_CONTROLLERS", DIRECTORY_BASE . "controllers" . DIRECTORY_SEPARATOR);
    define("DIRECTORY_MODELS", DIRECTORY_BASE . "models" . DIRECTORY_SEPARATOR);
    define("DIRECTORY_VIEWS", DIRECTORY_BASE . "views" . DIRECTORY_SEPARATOR);
    define("DIRECTORY_ASSETS", DIRECTORY_BASE . "assets" . DIRECTORY_SEPARATOR);
    define("DIRECTORY_MODULES", DIRECTORY_BASE . "modules" . DIRECTORY_SEPARATOR);

    spl_autoload_register(function($className){
        $className = explode("\\", $className);
        // Revisa si hay archivos sueltos en CONTROLLERS, MODELS, MODULES y VIEWS
        if(count($className) == 1) {
            foreach([
                    DIRECTORY_CONTROLLERS,
                    DIRECTORY_MODULES,
                    DIRECTORY_VIEWS
                ] as $dir){
                if(is_file($dir . $className[0] . ".php")){
                    require_once($dir . $className[0] . ".php");
                    return;
                }
            }
            if($className[0] == "Model" && is_file(DIRECTORY_MODELS . $className[0] . ".php")){
                    require_once(DIRECTORY_MODELS . $className[0] . ".php");
                    return;
            }
        }

        switch($className[0]){
            case "Controller":
                $dir = DIRECTORY_CONTROLLERS;
                break;
            case "Model":
                $dir = DIRECTORY_MODELS;
                break;
            case "View":
                $dir = DIRECTORY_VIEWS;
                break;
                
        }
        
        if(isset($dir) && $className[0] != "Module"){
            unset($className[0]);
            $className = implode(DIRECTORY_SEPARATOR, $className);
            if(is_file($dir . $className . ".php")){
                require_once($dir . $className . ".php");
                return;
            }
        }
        
        
    });
    
    Bootstrap::init();

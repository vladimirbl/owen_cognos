<?php

    Routes::get('/inventario/productos', 'inventory|Items.index', true, ['INVENTORY', 'INVENTORY_ITEMS']);
    Routes::get('/inventario/productos/nuevo', 'inventory|Items.createForm', true, ['INVENTORY', 'INVENTORY_ITEMS']);
    Routes::post('/inventario/productos/nuevo', 'inventory|Items.create', true, ['INVENTORY', 'INVENTORY_ITEMS']);
    Routes::post('/inventario/productos/editar', 'inventory|Items.edit', true, ['INVENTORY', 'INVENTORY_ITEMS']);
    Routes::post('/inventario/productos/eliminar', 'inventory|Items.delete', true, ['INVENTORY', 'INVENTORY_ITEMS']);

    Routes::get('/inventario/foto', 'inventory|Fotos.readFile', true, ['INVENTORY', 'INVENTORY_ITEMS']);
    Routes::post('/inventario/foto', 'inventory|Fotos.uploadFile', true, ['INVENTORY', 'INVENTORY_ITEMS']);

    Routes::get('/inventario/almacenes', 'inventory|Warehouses.index', true, ['INVENTORY', 'INVENTORY_WAREHOUSES']);
    Routes::get('/inventario/almacenes/nuevo', 'inventory|Warehouses.createForm', true, ['INVENTORY', 'INVENTORY_WAREHOUSES']);
    Routes::post('/inventario/almacenes/nuevo', 'inventory|Warehouses.create', true, ['INVENTORY', 'INVENTORY_WAREHOUSES']);
    Routes::post('/inventario/almacenes/editar', 'inventory|Warehouses.edit', true, ['INVENTORY', 'INVENTORY_WAREHOUSES']);
    Routes::post('/inventario/almacenes/eliminar', 'inventory|Warehouses.delete', true, ['INVENTORY', 'INVENTORY_WAREHOUSES']);

    Routes::get('/inventario/registros', 'inventory|Registers.index', true, ['INVENTORY', 'INVENTORY_REGISTERS']);
    Routes::get('/inventario/registros/nuevo', 'inventory|Registers.createForm', true, ['INVENTORY', 'INVENTORY_REGISTERS']);
    Routes::post('/inventario/registros/nuevo', 'inventory|Registers.create', true, ['INVENTORY', 'INVENTORY_REGISTERS']);
    Routes::post('/inventario/registros/editar', 'inventory|Registers.edit', true, ['INVENTORY', 'INVENTORY_REGISTERS']);
    Routes::post('/inventario/registros/eliminar', 'inventory|Registers.delete', true, ['INVENTORY', 'INVENTORY_REGISTERS']);

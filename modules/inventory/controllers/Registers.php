<?php

    namespace Module\Inventory\Controller;

    use \Module\Inventory\Model\Registers as RegistersDB;
    use \Module\Inventory\Model\Items as ItemsDB;
    use \Module\Inventory\Model\Warehouses as WarehousesDB;

    use \Controller;
    use \View;

    class Registers extends Controller {
        private $registersModel;
        private $itemsModel;
        private $warehousesModel;

        public function __construct() {
            $this->registersModel = new RegistersDB;
            $this->itemsModel = new ItemsDB;
            $this->warehousesModel = new WarehousesDB;
        }

        public function index(){
            if(isset($_GET['id'])){
                $datosRegistro = $this->registersModel->getRegister($_GET['id']);

                $itemsList = $this->itemsModel->getItems();
                $warehousesList = $this->warehousesModel->getWarehouses();

                View::show('main/layout', [
                    'title' => 'Información del registro ' . trim($datosRegistro['register_id']),
                    'datosRegistro' => $datosRegistro,
                    'itemsList' => $itemsList,
                    'warehousesList' => $warehousesList
                ], [
                    'main' => 'inventory|editRegister'
                ]);
            } else {
                $listaRegistros = $this->registersModel->getRegisters();

                View::show('main/layout', [
                    'title' => 'Lista de registros de ingreso',
                    'listaRegistros' => $listaRegistros
                ], [
                    'main' => 'inventory|registers'
                ]);
            }
        }

        public function createForm(){
            $itemsList = $this->itemsModel->getItems();
            $warehousesList = $this->warehousesModel->getWarehouses();

            View::show('main/layout', [
                'title' => 'Nuevo Registro',
                'itemsList' => $itemsList,
                'warehousesList' => $warehousesList
            ], [
                'main' => 'inventory|newRegister'
            ]);
        }

        public function create(){
            $newId = $this->registersModel->insertRegister($_POST['item_id'], $_POST['warehouse_id'], $_POST['register_description'], $_POST['register_amount'], $_POST['register_unit_cost']);

            if($newId !== false){
                echo json_encode(['response' => 'done']);
                return;
            }

            echo json_encode(['response' => 'error']);
        }

        public function edit(){
            $this->registersModel->updateRegister($_POST['register_id'], $_POST['item_id'], $_POST['warehouse_id'], $_POST['register_description'], $_POST['register_amount'], $_POST['register_unit_cost']);

            echo json_encode(['response' => 'done']);
        }
        
        public function delete(){
            $registerId = $_POST['id'];
            
            $this->registersModel->deleteRegister($registerId);

            echo json_encode(["response" => "done"]);
        }
    }


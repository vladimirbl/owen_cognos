<?php

    namespace Module\Inventory\Controller;

    use \Module\Inventory\Model\Items as ItemsDB;
    use \Module\Inventory\Model\Pictures as PicturesDB;

    use \Controller;
    use \View;

    class Items extends Controller {
        private $itemsModel;
        private $picturesModel;

        public function __construct() {
            $this->itemsModel = new ItemsDB;
            $this->picturesModel = new PicturesDB;
        }

        public function index(){
            if(isset($_GET['id'])){
                $imagenesLista = [];

                $datosProducto = $this->itemsModel->getItem($_GET['id']);

                $listaPictures = $this->picturesModel->getPicturesByItem($_GET['id']);
                foreach($listaPictures as $val) $imagenesLista[] = $val['picture_code'];

                View::show('main/layout', [
                    'title' => 'Información del producto ' . trim($datosProducto['item_name']),
                    'datosProducto' => $datosProducto,
                    'imagenesLista' => $imagenesLista
                ], [
                    'main' => 'inventory|editItem'
                ]);
            } else {
                $listaProductos = $this->itemsModel->getItems();

                View::show('main/layout', [
                    'title' => 'Lista de productos',
                    'listaProductos' => $listaProductos
                ], [
                    'main' => 'inventory|items'
                ]);
            }
        }

        public function createForm(){
            View::show('main/layout', [
                'title' => 'Nuevo Producto'
            ], [
                'main' => 'inventory|newItem'
            ]);
        }

        public function create(){
            $newId = $this->itemsModel->insertItem($_POST['item_code'], $_POST['item_name'], $_POST['item_description'], $_POST['item_color'], $_POST['item_marca']);

            if($newId !== false){
                if(isset($_POST['item_pictures'])){
                    foreach($_POST['item_pictures'] as $val) {
                        $this->picturesModel->insertPicture($newId, $val);
                    }
                }

                echo json_encode(['response' => 'done']);
                return;
            }

            echo json_encode(['response' => 'error']);
        }

        public function edit(){
            $this->itemsModel->updateItem($_POST['item_id'], $_POST['item_code'], $_POST['item_name'], $_POST['item_description'], $_POST['item_color'], $_POST['item_marca']);

            if(isset($_POST['item_pictures'])){
                foreach((array) $_POST['item_pictures'] as $val) {
                    $this->picturesModel->insertPicture($_POST['item_id'], $val);
                }
            }

            echo json_encode(['response' => 'done']);
        }
        
        public function delete(){
            $itemId = $_POST['id'];
            
            $this->picturesModel->deletePictureByItem($itemId);
            $this->itemsModel->deleteItem($itemId);

            echo json_encode(["response" => "done"]);
        }
    }

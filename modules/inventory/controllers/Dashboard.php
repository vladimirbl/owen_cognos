<?php

    namespace Module\Inventory\Controller;

    use \Module\Inventory\Model\Registers as RegistersDB;

    use \Controller;

    class Dashboard extends Controller {
        private $registerModel;

        public function __construct() {
            $this->registerModel = new RegistersDB;
        }

        public function getTotalPerWarehouse() {
            $totalPerW = $this->registerModel->getTotalItemsPerWarehouse();

            $totalList = [];

            foreach($totalPerW as $val) {
                $totalList[] = [$val['description'], (int) $val['total']];
            }

            return json_encode($totalList);
        }

        public function getTotalCostAll() {
            setlocale(LC_MONETARY, 'es_BO');
            $totalCost = $this->registerModel->getTotalCostAll();

            return 'Bs. ' . number_format($totalCost['total'], 2);
        }
    }

<?php

    namespace Module\Inventory\Controller;

    use \Module\Inventory\Model\Warehouses as WarehousesDB;

    use \Controller;
    use \View;

    class Warehouses extends Controller {
        private $warehousesModel;

        public function __construct() {
            $this->warehousesModel = new WarehousesDB;
        }

        public function index(){
            if(isset($_GET['id'])){
                $datosAlmacen = $this->warehousesModel->getWarehouse($_GET['id']);

                View::show('main/layout', [
                    'title' => 'Información del almacén ' . trim($datosAlmacen['warehouse_code']),
                    'datosAlmacen' => $datosAlmacen
                ], [
                    'main' => 'inventory|editWarehouse'
                ]);
            } else {
                $listaAlmacenes = $this->warehousesModel->getWarehouses();

                View::show('main/layout', [
                    'title' => 'Lista de almacenes',
                    'listaAlmacenes' => $listaAlmacenes
                ], [
                    'main' => 'inventory|warehouses'
                ]);
            }
        }

        public function createForm(){
            View::show('main/layout', [
                'title' => 'Nuevo Almacén'
            ], [
                'main' => 'inventory|newWarehouse'
            ]);
        }

        public function create(){
            $newId = $this->warehousesModel->insertWarehouse($_POST['warehouse_code'], $_POST['warehouse_description']);

            if($newId !== false){
                echo json_encode(['response' => 'done']);
                return;
            }

            echo json_encode(['response' => 'error']);
        }

        public function edit(){
            $this->warehousesModel->updateWarehouse($_POST['warehouse_id'], $_POST['warehouse_code'], $_POST['warehouse_description']);

            echo json_encode(['response' => 'done']);
        }
        
        public function delete(){
            $warehouseId = $_POST['id'];
            
            $this->warehousesModel->deleteWarehouse($warehouseId);

            echo json_encode(["response" => "done"]);
        }
    }


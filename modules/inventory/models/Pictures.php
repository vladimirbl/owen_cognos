<?php
    namespace Module\Inventory\Model;

    use \Model;

    class Pictures extends Model {
        
        public function getPicturesByItem($itemId){
            $query = $this->connection->prepare("
                SELECT
                  picture_code
                FROM inventory_pictures
                WHERE item_id = :item_id
            ");

            $query->bindValue(":item_id", $itemId);

            $query->execute();

            return $query->fetchAll();
        }
        
        public function insertPicture($itemId, $pictureCode){
            $query = $this->connection->prepare("
                INSERT INTO inventory_pictures (
                  item_id,
                  picture_code
                ) VALUES (
                  :item_id,
                  :picture_code
                )
            ");

            $query->bindValue(":item_id", $itemId);
            $query->bindValue(":picture_code", $pictureCode);

            $query->execute();
            
            return $this->connection->lastInsertId();
        }
        
        public function deletePicture($id){
            $query = $this->connection->prepare("
                DELETE FROM inventory_pictures
                WHERE picture_id = :id
            ");
                                                 
            $query->bindValue(":id", $id);
            $query->execute();

            return true;
        }
        
        public function deletePictureByItem($id){
            $query = $this->connection->prepare("
                DELETE FROM inventory_pictures
                WHERE item_id = :id
            ");
                                                 
            $query->bindValue(":id", $id);
            $query->execute();

            return true;
        }

    }

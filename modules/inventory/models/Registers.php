<?php
    namespace Module\Inventory\Model;

    use \Model;

    class Registers extends Model {
        
        public function getRegisters(){
            $query = $this->connection->prepare("
                SELECT
                  r.register_id,
                  i.item_name,
                  w.warehouse_code,
                  r.register_description,
                  r.register_amount,
                  r.register_unit_cost,
                  r.register_date
                FROM inventory_registers AS r
                INNER JOIN inventory_items AS i
                  ON i.item_id = r.item_id
                INNER JOIN inventory_warehouses AS w
                  ON w.warehouse_id = r.warehouse_id
            ");

            $query->execute();

            return $query->fetchAll();
        }
        
        public function getRegistersByItemWarehouse($itemId = '-1', $warehouseId = '-1'){
            $query = $this->connection->prepare("
                SELECT
                  register_description,
                  register_amount,
                  register_unit_cost,
                  register_date
                FROM inventory_registers
                WHERE item_id = :item_id
                  AND warehouse_id = :warehouse_id
            ");

            $query->bindValue(":item_id", $itemId);
            $query->bindValue(":warehouse_id", $warehouseId);

            $query->execute();

            return $query->fetchAll();
        }
        
        public function getRegister($registerId){
            $query = $this->connection->prepare("
                SELECT
                  register_id,
                  item_id,
                  warehouse_id,
                  register_description,
                  register_amount,
                  register_unit_cost,
                  register_date
                FROM inventory_registers
                WHERE register_id = :register_id
            ");

            $query->bindValue(":register_id", $registerId);

            $query->execute();

            return $query->fetch();
        }

        public function insertRegister($itemId, $warehouseId, $registerDescription, $registerAmount, $registerUnitCost) {
            $query = $this->connection->prepare("
                INSERT INTO inventory_registers (
                  item_id,
                  warehouse_id,
                  register_description,
                  register_amount,
                  register_unit_cost
                ) VALUES (
                  :item_id,
                  :warehouse_id,
                  :register_description,
                  :register_amount,
                  :register_unit_cost
                )
            ");

            $query->bindValue(":item_id", $itemId);
            $query->bindValue(":warehouse_id", $warehouseId);
            $query->bindValue(":register_description", $registerDescription);
            $query->bindValue(":register_amount", $registerAmount);
            $query->bindValue(":register_unit_cost", $registerUnitCost);

            $query->execute();
            
            return $this->connection->lastInsertId();
        }
        
        public function updateRegister($registerId, $itemId, $warehouseId, $registerDescription, $registerAmount, $registerUnitCost){
            $query = $this->connection->prepare("
                UPDATE inventory_registers
                SET
                  item_id = :item_id,
                  warehouse_id = :warehouse_id,
                  register_description = :register_description,
                  register_amount = :register_amount,
                  register_unit_cost = :register_unit_cost
                WHERE register_id = :register_id
            ");

            $query->bindValue(":register_id", $registerId);

            $query->bindValue(":item_id", $itemId);
            $query->bindValue(":warehouse_id", $warehouseId);
            $query->bindValue(":register_description", $registerDescription);
            $query->bindValue(":register_amount", $registerAmount);
            $query->bindValue(":register_unit_cost", $registerUnitCost);

            $query->execute();

            return true;
        }
        
        public function deleteRegister($id){
            $query = $this->connection->prepare("
                DELETE FROM inventory_registers
                WHERE register_id = :register_id
            ");
                                                 
            $query->bindValue(":register_id", $id);
            $query->execute();

            return true;
        }

        public function getTotalItemsPerWarehouse() {
            $query = $this->connection->prepare("
              SELECT
                CONCAT_WS(': ', w.warehouse_code, w.warehouse_description) AS description,
                SUM(r.register_amount) AS total
              FROM inventory_registers AS r
              INNER JOIN inventory_warehouses AS w
                ON r.warehouse_id = w.warehouse_id
              GROUP BY r.warehouse_id
            ");
                                                 
            $query->execute();

            return $query->fetchAll();
        }

        public function getTotalCostAll() {
            $query = $this->connection->prepare("
              SELECT
                SUM(register_amount * register_unit_cost) AS total
              FROM inventory_registers
            ");
                                                 
            $query->execute();

            return $query->fetch();
        }

    }

<?php
    namespace Module\Inventory\Model;

    use \Model;

    class Items extends Model {
        
        public function getItems(){
            $query = $this->connection->prepare("
                SELECT
                  i.item_id,
                  i.item_code,
                  i.item_name,
                  i.item_description,
                  i.item_color,
                  i.item_marca,
                  count(p.item_id) AS item_pictures_total
                FROM inventory_items AS i
                LEFT JOIN inventory_pictures AS p
                  ON p.item_id = i.item_id
                GROUP BY p.item_id
            ");

            $query->execute();

            return $query->fetchAll();
        }
        
        public function getItem($id){
            $query = $this->connection->prepare("
                SELECT
                  item_id,
                  item_code,
                  item_name,
                  item_description,
                  item_color,
                  item_marca
                FROM inventory_items
                WHERE item_id = :item_id
            ");

            $query->bindValue(":item_id", $id);

            $query->execute();

            return $query->fetch();
        }
        
        public function insertItem($itemCode, $itemName, $itemDescription, $itemColor, $itemMarca){
            $query = $this->connection->prepare("
                INSERT INTO inventory_items (
                  item_code,
                  item_name,
                  item_description,
                  item_color,
                  item_marca
                ) VALUES (
                  :item_code,
                  :item_name,
                  :item_description,
                  :item_color,
                  :item_marca
                )
            ");

            $query->bindValue(":item_code", $itemCode);
            $query->bindValue(":item_name", $itemName);
            $query->bindValue(":item_description", $itemDescription);
            $query->bindValue(":item_color", $itemColor);
            $query->bindValue(":item_marca", $itemMarca);

            $query->execute();
            
            return $this->connection->lastInsertId();
        }
        
        public function updateItem($itemId, $itemCode, $itemName, $itemDescription, $itemColor, $itemMarca){
            $query = $this->connection->prepare("
                UPDATE inventory_items
                SET 
                  item_code = :item_code,
                  item_name = :item_name,
                  item_description = :item_description,
                  item_color = :item_color,
                  item_marca = :item_marca
                WHERE item_id = :item_id
            ");

            $query->bindValue(":item_id", $itemId);

            $query->bindValue(":item_code", $itemCode);
            $query->bindValue(":item_name", $itemName);
            $query->bindValue(":item_description", $itemDescription);
            $query->bindValue(":item_color", $itemColor);
            $query->bindValue(":item_marca", $itemMarca);

            $query->execute();

            return true;
        }
        
        public function deleteItem($id){
            $query = $this->connection->prepare("
                DELETE FROM inventory_items
                WHERE item_id = :id
            ");
                                                 
            $query->bindValue(":id", $id);
            $query->execute();

            return true;
        }

    }

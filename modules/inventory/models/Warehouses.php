<?php
    namespace Module\Inventory\Model;

    use \Model;

    class Warehouses extends Model {
        
        public function getWarehouses(){
            $query = $this->connection->prepare("
                SELECT
                  warehouse_id,
                  warehouse_code,
                  warehouse_description
                FROM inventory_warehouses
            ");

            $query->execute();

            return $query->fetchAll();
        }
        
        public function getWarehouse($warehouseId){
            $query = $this->connection->prepare("
                SELECT
                  warehouse_id,
                  warehouse_code,
                  warehouse_description
                FROM inventory_warehouses
                WHERE warehouse_id = :warehouse_id
            ");

            $query->bindValue(":warehouse_id", $warehouseId);

            $query->execute();

            return $query->fetch();
        }

        public function insertWarehouse($warehouseCode, $warehouseDescription){
            $query = $this->connection->prepare("
                INSERT INTO inventory_warehouses (
                  warehouse_code,
                  warehouse_description
                ) VALUES (
                  :warehouse_code,
                  :warehouse_description
                )
            ");

            $query->bindValue(":warehouse_code", $warehouseCode);
            $query->bindValue(":warehouse_description", $warehouseDescription);

            $query->execute();
            
            return $this->connection->lastInsertId();
        }
        
        public function updateWarehouse($warehouseId, $warehouseCode, $warehouseDescription){
            $query = $this->connection->prepare("
                UPDATE inventory_warehouses
                SET
                  warehouse_code = :warehouse_code,
                  warehouse_description = :warehouse_description
                WHERE warehouse_id = :warehouse_id
            ");

            $query->bindValue(":warehouse_id", $warehouseId);

            $query->bindValue(":warehouse_code", $warehouseCode);
            $query->bindValue(":warehouse_description", $warehouseDescription);

            $query->execute();

            return true;
        }
        
        public function deleteWarehouse($id){
            $query = $this->connection->prepare("
                DELETE FROM inventory_warehouses
                WHERE warehouse_id = :warehouse_id
            ");
                                                 
            $query->bindValue(":warehouse_id", $id);
            $query->execute();

            return true;
        }

    }

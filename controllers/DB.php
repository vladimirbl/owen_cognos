<?php

    use \Utils;

    /**
     * Maneja los elementos principales de conexión a BD
     *
     * @author Vladimir
     * @version 0.1
     */
    class DB {
        static private $connection = null;
        private $engine;
        private $host;
        private $port;
        private $name;
        private $password;
        private $database;
        
        public function __construct(){
            $this->getConnection();
        }
        
        public function getConnection(){
            if(self::$connection === null){
                $this->getConfiguration();
                if($this->engine != ""){
                    $chain  = $this->engine . ":";
                    $chain .= "host=" . $this->host;
                    $chain .= ";port=" . $this->port;
                    $chain .= ";dbname=" . $this->database;
                    
                    try{
                        self::$connection = new PDO($chain, $this->name, $this->password);
                    }catch(Exception $e){
                        error_log($e->getMessage());
                    }
                }
            }
            
            return self::$connection;
        }
        
        /**
         * Obtiene los parámetros de base de datos del archivo
         * config.json
         *
         * @author Vladimir
         */
        private function getConfiguration(){
            $config = Utils::getConfiguration('db');

            $this->engine = $config['engine'];
            $this->host = $config['host'];
            $this->port = $config['port'];
            $this->name = $config['name'];
            $this->password = $config['password'];
            $this->database = $config['database'];
        }
    }

<?php

    use \Model\Modules as ModulesDB;
    use \Model\Permissions as PermissionsDB;

    class Modules {
        static private $modules = [];

        static public function init(){
            $modules = self::getActivedModules();

            foreach($modules as $val) {
                if(is_file(DIRECTORY_MODULES . $val . DIRECTORY_SEPARATOR . 'routes.php')){
                    include_once(DIRECTORY_MODULES . $val . DIRECTORY_SEPARATOR . 'routes.php');
                }
            }

            self::createAutoload();
        }

        static public function verifyPermissions(string $code) {
            $permisos = [];
            $permissionModel = new PermissionsDB;

            if(is_file(DIRECTORY_MODULES . $code . DIRECTORY_SEPARATOR . 'permisos.json')){
                $permisos = file_get_contents(DIRECTORY_MODULES . $code . DIRECTORY_SEPARATOR . 'permisos.json');
                $permisos = json_decode($permisos, true);
            }

            $allPermissions = $permissionModel->getPermissions();

            $permissionsList = [];
            foreach($allPermissions as $val)
                $permissionsList[] = $val['permission_code'];

            foreach($permisos as $val){
                if(!in_array($val['code'], $permissionsList)){
                    $permissionModel->insertPermission($val['code'], $val['description']);
                }
            }
        }

        static public function getSidebar():string {
            $modules = self::getActivedModules();
            $content = "";

            foreach($modules as $val){
                if(is_file(DIRECTORY_MODULES . $val . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'sidebar.owen')){
                    $content .= file_get_contents(DIRECTORY_MODULES . $val . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'sidebar.owen');
                }
            }

            return $content;
        }

        static public function getDashboard():string {
            $modules = self::getActivedModules();
            $content = "";

            foreach($modules as $val){
                if(is_file(DIRECTORY_MODULES . $val . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'dashboard.owen')){
                    $content .= file_get_contents(DIRECTORY_MODULES . $val . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . 'dashboard.owen');
                }
            }

            return $content;
        }

        static private function createAutoload() {
            spl_autoload_register(function($className){
                $className = explode("\\", $className);

                if($className[0] == "Module" && count($className) > 3) {
                    $dir = DIRECTORY_MODULES . strtolower($className[1]) . DIRECTORY_SEPARATOR;

                    switch($className[2]){
                        case "Controller": $dir .= "controllers" . DIRECTORY_SEPARATOR; break;
                        case "Model":      $dir .= "models" . DIRECTORY_SEPARATOR;      break;
                        case "View":       $dir .= "views" . DIRECTORY_SEPARATOR;       break;
                    }

                    unset($className[0]);
                    unset($className[1]);
                    unset($className[2]);

                    $className = implode(DIRECTORY_SEPARATOR, $className);

                    if(is_file($dir . $className . ".php")){
                        require_once($dir . $className . ".php");
                        return;
                    }
                }
            });
        }

        static public function getActivedModules():array {
            $modulesModel = new ModulesDB;
            $allModules = self::getModulesFromFolder();

            $modulesCode = [];

            foreach ($allModules as $value)
                $modulesCode[] = $value['code'];

            $listModules = $modulesModel->getActiveModules();

            $activeModulesList = [];

            foreach($listModules as $val) {
                if(in_array($val['module_code'], $modulesCode)) {
                    $activeModulesList[] = $val['module_code'];
                }
            }

            return $activeModulesList;
        }

        static public function getModulesFromFolder():array {
            if(count(self::$modules) == 0){
                $listDir = scandir(DIRECTORY_MODULES);

                foreach($listDir as $val){
                    if(is_file(DIRECTORY_MODULES . $val . DIRECTORY_SEPARATOR . 'readme')){
                        $readme = file_get_contents(DIRECTORY_MODULES . $val . DIRECTORY_SEPARATOR . 'readme');
                        self::$modules[] = [
                            "code" => $val,
                            "description" => $readme
                        ];
                    }
                }
            }

            return self::$modules;
        }
    }

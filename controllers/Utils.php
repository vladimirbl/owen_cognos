<?php

    class Utils {
        static public function pr($array):void {
            if(is_array($array)){
                echo "<pre>";
                print_r($array);
                echo "</pre>";
            } else if(is_object($array)){
                echo "<pre>";
                var_dump($array);
                echo "</pre>";
            } else {
                echo $array;
            }
        }

        static public function getConfiguration(string $element):array {
            $config = file_get_contents(DIRECTORY_BASE . 'config.json');
            $config = json_decode($config, true);

            if(isset($config[$element])) return $config[$element];

            return [];
        }

        static public function P($permission):bool {
            @session_start();

            if(is_string($permission)) $permission = [$permission];

            if(isset($_SESSION['user_permissions'])){
                foreach($permission as $val){
                    if(!in_array($val, $_SESSION['user_permissions']))
                        return false;
                }
                return true;
            }
            return false;
        }
    }

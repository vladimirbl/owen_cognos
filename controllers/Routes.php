<?php

    use \Controller\Authentication\Authentication;

    class Routes {
        static $routesGET = [];
        static $routesPOST = [];

        static function evalRoute(){
            $reqMethod = $_SERVER['REQUEST_METHOD'] == 'POST' ? 'POST' : 'GET';
            $reqRoutes = explode("?", $_SERVER['REQUEST_URI'])[0];

            if($reqMethod == 'GET' && isset(self::$routesGET[$reqRoutes])){
                $controller = explode('|', self::$routesGET[$reqRoutes]);
                if(count($controller) > 1){
                    $module = ucfirst($controller[0]);
                    $controller = explode('.', $controller[1]);
                    $controller[0] = str_replace('/', '\\', $controller[0]);
                    eval('$objController = new \\Module\\' . $module . '\\Controller\\' . $controller[0] . ';');
                    eval('$objController->' . $controller[1] . '();');
                } else {
                    $controller = explode('.', $controller[0]);
                    $controller[0] = str_replace('/', '\\', $controller[0]);
                    eval('$objController = new \\Controller\\' . $controller[0] . ';');
                    eval('$objController->' . $controller[1] . '();');
                }
                return;
            }


            if($reqMethod == 'POST' && isset(self::$routesPOST[$reqRoutes])){
                $controller = explode('|', self::$routesPOST[$reqRoutes]);
                if(count($controller) > 1){
                    $module = ucfirst($controller[0]);
                    $controller = explode('.', $controller[1]);
                    $controller[0] = str_replace('/', '\\', $controller[0]);
                    eval('$objController = new \\Module\\' . $module . '\\Controller\\' . $controller[0] . ';');
                    eval('$objController->' . $controller[1] . '();');
                } else {
                    $controller = explode('.', $controller[0]);
                    $controller[0] = str_replace('/', '\\', $controller[0]);
                    eval('$objController = new \\Controller\\' . $controller[0] . ';');
                    eval('$objController->' . $controller[1] . '();');
                }
                return;
            }

            View::show('index/error');
        }

        public function get(string $route, string $controller, bool $auth = false, array $permission = []){
            if(($auth && Authentication::isAuthenticated() && Utils::P($permission)) || !$auth)
                self::$routesGET[$route] = $controller;
        }

        public function post(string $route, string $controller, bool $auth = false, array $permission = []){
            if(($auth && Authentication::isAuthenticated() && Utils::P($permission)) || !$auth)
                self::$routesPOST[$route] = $controller;
        }
    }

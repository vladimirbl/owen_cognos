<?php

    class Bootstrap {
        static final function init(){
            require(DIRECTORY_BASE . 'routes.php');
            Modules::init();
            Routes::evalRoute();
        }
    }

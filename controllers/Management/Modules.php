<?php

    namespace Controller\Management;

    use \Model\Modules as ModulesDB;
    use \Controller;
    use \Modules as MainModules;
    use \View;

    class Modules extends Controller {
        
        private $moduleModel;
        
        public function __construct(){
            $this->moduleModel = new ModulesDB;
        }

        public function index(){
            $activeModules = MainModules::getActivedModules();
            $allModules = MainModules::getModulesFromFolder();

            $queryModules = $this->moduleModel->getModules();

            $modulesList = [];
            foreach($queryModules as $val) $modulesList[] = $val['module_code'];

            foreach($allModules as $val) {
                if(!in_array($val['code'], $modulesList))
                    $this->moduleModel->insertModule($val['code'], $val['description']);
            }

            View::show('main/layout', [
                'title' => 'Lista de módulos',
                'activeModules' => $activeModules,
                'allModules' => $allModules
            ], [
                'main' => 'management/modules'
            ]);
        }

        public function changeStatus(){
            $code = isset($_POST['code']) ? $_POST['code'] : '';
            $status = isset($_POST['status']) ? $_POST['status'] : 0;

            $queryModules = $this->moduleModel->changeStatus($code, $status);

            if($status == 1) {
                MainModules::verifyPermissions($code);
            }

            echo json_encode(['response' => 'done']);
        }
    }

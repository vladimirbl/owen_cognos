<?php

    namespace Controller\Management;
    
    use \Model\Users as UsersDB;
    use \Model\Permissions as PermissionsDB;
    use \Model\UsersPermissions as UsersPermissionsDB;
    use \Controller;
    use \View;

    class Users extends Controller {
        
        private $userModel;
        private $permissionModel;
        private $userPermissionModel;
        
        public function __construct(){
            $this->userModel = new UsersDB;
            $this->permissionModel = new PermissionsDB;
            $this->userPermissionModel = new UsersPermissionsDB;
        }

        public function index(){
            if(isset($_GET['id'])){
                $permisosUsuario = [];

                $datosUsuario = $this->userModel->getUser($_GET['id']);

                $listaPermisosUsuario = $this->userPermissionModel->getPermissionsByUser($_GET['id']);
                foreach($listaPermisosUsuario as $val) $permisosUsuario[] = $val['permission_code'];

                $permisos = $this->permissionModel->getPermissions();

                View::show('main/layout', [
                    'title' => 'Información del usuario ' . trim($datosUsuario['user_name'] . ' ' . $datosUsuario['user_lastname']),
                    'datosUsuario' => $datosUsuario,
                    'permisosUsuario' => $permisosUsuario,
                    'permisos' => $permisos
                ], [
                    'main' => 'management/editUser'
                ]);
            } else {
                $listaUsuarios = $this->userModel->getUsers();

                View::show('main/layout', [
                    'title' => 'Lista de usuarios',
                    'listaUsuarios' => $listaUsuarios,
                    'user' => $_SESSION['user_fullname']
                ], [
                    'main' => 'management/users'
                ]);
            }
        }
        
        public function createForm(){
            $permisos = $this->permissionModel->getPermissions();

            View::show('main/layout', [
                'title' => 'Nuevo usuario',
                'permisos' => $permisos
            ], [
                'main' => 'management/newUser'
            ]);
        }
        
        public function create(){
            $name = $_POST['user_name'];
            $lastname = $_POST['user_lastname'];
            $email = $_POST['user_email'];
            $password = $_POST['user_password'];

            $newId = $this->userModel->insertUser($name, $lastname, $email, $password);

            if($newId !== false){
                foreach($_POST['permission'] as $key => $val){
                    $this->userPermissionModel->insertUserPermission($newId, $key);
                }

                echo json_encode(["response" => "done"]);
                return;
            }
            
            echo json_encode(["response" => "error"]);
        }
        
        public function delete(){
            $userId = $_POST['id'];
            
            $this->userPermissionModel->deleteByUser($userId);
            $this->userModel->deleteUser($userId);

            echo json_encode(["response" => "done"]);
        }
        
        public function edit(){
            $id = $_POST['user_id'];
            $name = $_POST['user_name'];
            $lastname = $_POST['user_lastname'];
            $email = $_POST['user_email'];
            $password = trim($_POST['user_password']);
            $status = isset($_POST['user_status']) ? 1 : 0;
            $permisosUsuario = [];

            $this->userModel->updateUser($id, $name, $lastname, $email, $status, $password);

            $listaPermisosUsuario = $this->userPermissionModel->getPermissionsByUser($id);
            foreach($listaPermisosUsuario as $val) $permisosUsuario[] = $val['permission_code'];

            foreach($_POST['permission'] as $key => $val){
                $i = array_search($key, $permisosUsuario);
                if($i !== false){
                    unset($_POST['permission'][$key]);
                    unset($permisosUsuario[$i]);
                }
            }

            foreach($_POST['permission'] as $key => $val){
                $this->userPermissionModel->setPermissionToUser($id, $key);
            }

            foreach($permisosUsuario as $key => $val){
                $this->userPermissionModel->removePermissionToUser($id, $val);
            }

            echo json_encode(["response" => "done"]);
        }
    }

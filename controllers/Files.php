<?php

    abstract class Files {

        public $filetypes = [];

        public function uploadFile(){

            if($_FILES['file']['error'] == 0){
                $newId = uniqid();
                
                $finfo = new finfo(FILEINFO_MIME_TYPE);
                $fileContents = file_get_contents($_FILES['file']['tmp_name']);
                $mimeType = $finfo->buffer($fileContents);

                if(count($this->filetypes) > 0){
                    if(in_array($mimeType, $this->filetypes)){
                        move_uploaded_file($_FILES['file']['tmp_name'], DIRECTORY_ASSETS . $newId);
                        echo json_encode(['response' => $newId]);
                    } else {
                        echo json_encode(['error' => 'typeIncorrect']);
                    }
                } else {
                    move_uploaded_file($_FILES['file']['tmp_name'], DIRECTORY_ASSETS . $newId);
                    echo json_encode(['response' => $newId]);
                }
            } else {
                echo json_encode(['error' => $_FILES['file']['error']]);
            }

        }

        public function readFile(){
            $id = $_GET['id'];

            $id = str_replace("/", "", $id);
            $id = str_replace("\\", "", $id);

            $finfo = new finfo(FILEINFO_MIME_TYPE);

            if(is_file(DIRECTORY_ASSETS . $id)) {
                $fileContents = file_get_contents(DIRECTORY_ASSETS . $id);
                $mimeType = $finfo->buffer($fileContents);
                header('Content-Type: ' . $mimeType);
                echo $fileContents;
            }

        }
    }
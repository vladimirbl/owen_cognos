<?php

    namespace Controller\Authentication;
    
    use \Model\Users;
    use \Model\UsersPermissions;
    use \Controller;
    use \View;

    class Authentication extends Controller {
        
        private $email;
        private $password;
        private $userModel;
        private $userPermissionModel;
        
        public function __construct(){
            $this->userModel = new Users;
            $this->userPermissionModel = new UsersPermissions;
        }
        
        public function checkAuthentication():void {
            $this->email = isset($_POST['email']) ? $_POST['email'] : '';
            $this->password = isset($_POST['password']) ? $_POST['password'] : '';
            
            $userInfo = $this->userModel->verifyLogin($this->email, $this->password);
            
            if($userInfo !== false){
                @session_start();
                $_SESSION['user_id'] = $userInfo['user_id'];
                $_SESSION['user_fullname'] = trim($userInfo['user_name'] . ' ' . $userInfo['user_lastname']);
                $_SESSION['user_permissions'] = [];

                $permissionList = $this->userPermissionModel->getPermissionsByUser($userInfo['user_id']);
                foreach($permissionList as $key => $val)
                    $_SESSION['user_permissions'][] = $val['permission_code'];
            }

            if($this->isAuthenticated()){
                header("location: /main");
            } else{
                View::show('index/index', ['mensaje' => 'Hubo un error al intentar acceder, por favor verifique.']);
            }
        }

        public function index():void {
            if($this->isAuthenticated()){
                header("location: /main");
                return;
            }
            View::show('index/index');
        }

        static public function isAuthenticated():bool {
            @session_start();
            return isset($_SESSION['user_id']) && !empty($_SESSION['user_id']);
        }

        public function logout():void {
            @session_start();
            unset($_SESSION['user_id']);
            @session_destroy();
            header("location: /");
        }
    }
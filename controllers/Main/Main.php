<?php

    namespace Controller\Main;
    
    use \Controller;
    use \View;

    class Main extends Controller {
        public function index(){
            View::show('main/layout', [
                'title' => 'Principal'
            ],
            [
                'main' => 'main/dashboard'
            ]);
        }
    }

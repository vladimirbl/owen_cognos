-- MySQL dump 10.16  Distrib 10.1.29-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: owen
-- ------------------------------------------------------
-- Server version	10.1.29-MariaDB-6

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `inventory_items`
--

DROP TABLE IF EXISTS `inventory_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_items` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_code` varchar(50) DEFAULT NULL,
  `item_name` varchar(50) DEFAULT NULL,
  `item_description` varchar(200) DEFAULT NULL,
  `item_color` varchar(50) DEFAULT NULL,
  `item_marca` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_items`
--

LOCK TABLES `inventory_items` WRITE;
/*!40000 ALTER TABLE `inventory_items` DISABLE KEYS */;
INSERT INTO `inventory_items` VALUES (10,'PR-123','Pantalones','Pantalones jeans azul oscuro','Azul','Patito');
/*!40000 ALTER TABLE `inventory_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_pictures`
--

DROP TABLE IF EXISTS `inventory_pictures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_pictures` (
  `picture_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `picture_code` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`picture_id`),
  KEY `FK_inventory_pictures_inventory_items` (`item_id`),
  CONSTRAINT `FK_inventory_pictures_inventory_items` FOREIGN KEY (`item_id`) REFERENCES `inventory_items` (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_pictures`
--

LOCK TABLES `inventory_pictures` WRITE;
/*!40000 ALTER TABLE `inventory_pictures` DISABLE KEYS */;
INSERT INTO `inventory_pictures` VALUES (15,10,'5b563a2b7079e');
/*!40000 ALTER TABLE `inventory_pictures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_registers`
--

DROP TABLE IF EXISTS `inventory_registers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_registers` (
  `register_id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `warehouse_id` int(11) DEFAULT NULL,
  `register_description` varchar(200) DEFAULT NULL,
  `register_amount` int(11) DEFAULT '0',
  `register_unit_cost` float DEFAULT '0',
  `register_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`register_id`),
  KEY `FK__inventory_items` (`item_id`),
  KEY `FK__inventory_warehouse` (`warehouse_id`),
  CONSTRAINT `FK__inventory_items` FOREIGN KEY (`item_id`) REFERENCES `inventory_items` (`item_id`),
  CONSTRAINT `FK__inventory_warehouse` FOREIGN KEY (`warehouse_id`) REFERENCES `inventory_warehouses` (`warehouse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_registers`
--

LOCK TABLES `inventory_registers` WRITE;
/*!40000 ALTER TABLE `inventory_registers` DISABLE KEYS */;
INSERT INTO `inventory_registers` VALUES (2,10,3,'test',23,12,'2018-07-23 20:33:49'),(3,10,2,'test',123,22,'2018-07-23 20:35:29'),(4,10,6,'Pantalones en proceso de fabricación',100,13,'2018-07-23 21:19:21');
/*!40000 ALTER TABLE `inventory_registers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventory_warehouses`
--

DROP TABLE IF EXISTS `inventory_warehouses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventory_warehouses` (
  `warehouse_id` int(11) NOT NULL AUTO_INCREMENT,
  `warehouse_code` varchar(50) DEFAULT NULL,
  `warehouse_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`warehouse_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventory_warehouses`
--

LOCK TABLES `inventory_warehouses` WRITE;
/*!40000 ALTER TABLE `inventory_warehouses` DISABLE KEYS */;
INSERT INTO `inventory_warehouses` VALUES (2,'ALM-001','Almacén de ropa'),(3,'PNT-004','Pantaloes'),(5,'MKN-004','Almacen de artículos comprados'),(6,'SSJ-235','Almacén de artículos en proceso');
/*!40000 ALTER TABLE `inventory_warehouses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modules`
--

DROP TABLE IF EXISTS `modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modules` (
  `module_code` varchar(50) NOT NULL,
  `module_description` varchar(200) DEFAULT NULL,
  `module_status` bit(1) NOT NULL,
  PRIMARY KEY (`module_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modules`
--

LOCK TABLES `modules` WRITE;
/*!40000 ALTER TABLE `modules` DISABLE KEYS */;
INSERT INTO `modules` VALUES ('inventory','Sistema de inventario básico',''),('sales','Sistema de ventas complementario al de inventario','');
/*!40000 ALTER TABLE `modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `permission_code` varchar(50) NOT NULL,
  `permission_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`permission_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES ('ADMIN','Administrador'),('INVENTORY','Permiso general de acceso'),('INVENTORY_ITEMS','Crear, modificar y eliminar productos'),('INVENTORY_REGISTER','Crear, modificar y eliminar registros de ingreso'),('INVENTORY_REGISTERS','Crear, modificar y eliminar registros de ingreso de productos'),('INVENTORY_WAREHOUSES','Crear, modificar y eliminar almacenes'),('SALES','Permiso general de acceso a ventas'),('SALES_LIST','Lista de ventas');
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(50) DEFAULT NULL,
  `user_lastname` varchar(50) DEFAULT NULL,
  `user_email` varchar(50) DEFAULT NULL,
  `user_password` varchar(100) DEFAULT NULL,
  `user_status` bit(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','','admin@owen.com','$2y$10$MLVgYNZBGmqnLwSK7CjV.OOZTqaYLusoxnFKUtymswdmp5oQhaiEm',''),(17,'Vladimir','Blaz','vladimir@owen.com','$2y$10$ndgjpW9nXKtBQnfP5brAcuSgjx.kTfb5V/WJVln4xonbOwggr7vSy','');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_permissions`
--

DROP TABLE IF EXISTS `users_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_permissions` (
  `user_id` int(11) DEFAULT NULL,
  `permission_code` varchar(50) DEFAULT NULL,
  KEY `FK__users` (`user_id`),
  KEY `FK__permissions` (`permission_code`),
  CONSTRAINT `FK__permissions` FOREIGN KEY (`permission_code`) REFERENCES `permissions` (`permission_code`),
  CONSTRAINT `FK__users` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_permissions`
--

LOCK TABLES `users_permissions` WRITE;
/*!40000 ALTER TABLE `users_permissions` DISABLE KEYS */;
INSERT INTO `users_permissions` VALUES (1,'ADMIN'),(17,'SALES_LIST'),(1,'INVENTORY_REGISTER'),(1,'INVENTORY_WAREHOUSES'),(1,'INVENTORY_REGISTERS'),(1,'INVENTORY_ITEMS'),(1,'INVENTORY'),(1,'SALES');
/*!40000 ALTER TABLE `users_permissions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-07-23 17:48:45

<?php
    namespace Model;

    use \Model;

    class UsersPermissions extends Model {
        
        public function getPermissionsByUser($id){
            $query = $this->connection->prepare("
                SELECT
                  p.permission_code,
                  p.permission_description
                FROM permissions AS p
                INNER JOIN users_permissions AS up
                  ON up.permission_code = p.permission_code
                WHERE up.user_id = :user_id
            ");
                                                 
            $query->bindValue(":user_id", $id);
            $query->execute();

            return $query->fetchAll();
        }
        
        public function insertUserPermission($userId, $permissionCode){
            $query = $this->connection->prepare("
                INSERT INTO users_permissions (
                  user_id,
                  permission_code
                ) VALUES (
                  :user_id,
                  :permission_code
                )
            ");
            
            $query->bindValue(":user_id", $userId);
            $query->bindValue(":permission_code", $permissionCode);

            $query->execute();

            return true;
        }
        
        public function deleteByUser($userId){
            $query = $this->connection->prepare("
                DELETE FROM users_permissions
                WHERE user_id = :user_id
            ");
            
            $query->bindValue(":user_id", $userId);

            $query->execute();

            return true;
        }
        
        public function deleteByPermission($permissionCode){
            $query = $this->connection->prepare("
                DELETE FROM users_permissions
                WHERE permission_code = :permission_code
            ");
            
            $query->bindValue(":permission_code", $permissionCode);

            $query->execute();

            return true;
        }
        
        public function setPermissionToUser($userId, $permissionCode){
            $query = $this->connection->prepare("
                INSERT INTO users_permissions (
                  user_id,
                  permission_code
                ) VALUES (
                  :user_id,
                  :permission_code
                )
            ");
                                                 
            $query->bindValue(":user_id", $userId);
            $query->bindValue(":permission_code", $permissionCode);
            $query->execute();

            return true;
        }
        
        public function removePermissionToUser($userId, $permissionCode){
            $query = $this->connection->prepare("
                DELETE FROM users_permissions
                WHERE user_id = :user_id
                  AND permission_code = :permission_code
            ");
                                                 
            $query->bindValue(":user_id", $userId);
            $query->bindValue(":permission_code", $permissionCode);
            $query->execute();

            return true;
        }
    }
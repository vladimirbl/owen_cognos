<?php
    namespace Model;

    use \Model;
    use \PDO;

    class Users extends Model {
        
        public function getUsers(){
            $query = $this->connection->prepare("
                SELECT
                  user_id,
                  user_name,
                  user_lastname,
                  user_email,
                  user_status
                FROM users
            ");

            $query->execute();

            return $query->fetchAll();
        }
        
        public function getUser(int $id){
            $query = $this->connection->prepare("
                SELECT
                  user_id,
                  user_name,
                  user_lastname,
                  user_email,
                  user_status
                FROM users
                WHERE user_id = :id
            ");

            $query->bindValue(":id", $id);
            $query->execute();

            return $query->fetch();
        }
        
        public function insertUser($name, $lastname, $email, $password){
            $query = $this->connection->prepare("
                INSERT INTO users (
                  user_name,
                  user_lastname,
                  user_email,
                  user_password,
                  user_status
                ) VALUES (
                  :user_name,
                  :user_lastname,
                  :user_email,
                  :user_password,
                  :user_status
                )
            ");
                                                 
            $query->bindValue(":user_name", $name);
            $query->bindValue(":user_lastname", $lastname);
            $query->bindValue(":user_email", $email);
            $query->bindValue(":user_password", password_hash($password, PASSWORD_DEFAULT));
            $query->bindValue(":user_status", 1, PDO::PARAM_INT);

            $query->execute();
            
            return $this->connection->lastInsertId();
        }
        
        public function updateUser($id, $name, $lastname, $email, $status, $password = ""){
            $subQuery = "";

            if($password != "") $subQuery = "user_password = :user_password,";

            $query = $this->connection->prepare("
                UPDATE users
                SET 
                  user_name = :user_name,
                  user_lastname = :user_lastname,
                  user_email = :user_email,
                  " . $subQuery . "
                  user_status = :user_status
                WHERE user_id = :user_id
            ");
                                                 
            $query->bindValue(":user_name", $name);
            $query->bindValue(":user_lastname", $lastname);
            $query->bindValue(":user_email", $email);
            $query->bindValue(":user_status", $status, PDO::PARAM_INT);
            $query->bindValue(":user_id", $id);

            if($password != "")
              $query->bindValue(":user_password", password_hash($password, PASSWORD_DEFAULT));

            $query->execute();

            return true;
        }
        
        public function deleteUser($id){
            $query = $this->connection->prepare("
                DELETE FROM users
                WHERE user_id = :id
            ");
                                                 
            $query->bindValue(":id", $id);
            $query->execute();

            return true;
        }

        public function verifyLogin($userEmail, $userPassword){
            $query = $this->connection->prepare("
                SELECT
                  user_id,
                  user_name,
                  user_lastname,
                  user_password
                FROM users
                WHERE user_email = :user_email
                  AND user_status = 1
            ");
            $query->bindValue(":user_email", $userEmail);
            $query->execute();
            
            $data = $query->fetch();
            
            if(password_verify($userPassword, $data['user_password'])){
                unset($data['user_password']);
                return $data;
            }
            
            return false;
        }
    }

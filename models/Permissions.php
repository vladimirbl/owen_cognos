<?php
    namespace Model;

    use \Model;

    class Permissions extends Model {
        
        public function getPermissions(){
            $query = $this->connection->prepare("
                SELECT
                  permission_code, 
                  permission_description
                FROM permissions
            ");
            $query->execute();

            return $query->fetchAll();
        }
        
        public function getPermission(string $id){
            $query = $this->connection->prepare("
                SELECT
                  permission_code, 
                  permission_description
                FROM permissions
                WHERE permission_code = :permission_code
            ");
            $query->bindValue(":permission_code", $id);
            $query->execute();

            return $query;
        }
        
        public function insertPermission($id, $description){
            $query = $this->connection->prepare("
                INSERT INTO permissions (
                  permission_code, 
                  permission_description
                ) VALUES (
                  :permission_code,
                  :permission_description
                )
            ");
                                                 
            $query->bindValue(":permission_code", $id);
            $query->bindValue(":permission_description", $description);
            $query->execute();

            return true;
        }
        
        public function updatePermission($id, $description){
            $query = $this->connection->prepare("
                UPDATE permissions
                SET
                  permission_description = :permission_description
                WHERE permission_code = :permission_code
            ");
                                                 
            $query->bindValue(":permission_code", $id);
            $query->bindValue(":permission_description", $description);
            $query->execute();

            return true;
        }
        
        public function deletePermission($id){
            $query = $this->connection->prepare("
                DELETE FROM permissions
                WHERE permission_code = :permission_code
            ");
                                                 
            $query->bindValue(":permission_code", $id);
            $query->execute();

            return true;
        }
    }

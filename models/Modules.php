<?php

    namespace Model;

    use \PDO;

    class Modules extends \Model {
        
        public function getModules(){
            $query = $this->connection->prepare("
                SELECT
                  module_code,
                  module_description,
                  module_status
                FROM modules
            ");
            $query->execute();

            return $query->fetchAll();
        }

        public function getActiveModules(){
            $query = $this->connection->prepare("
                SELECT
                  module_code,
                  module_description,
                  module_status
                FROM modules
                WHERE module_status = 1
            ");
            $query->execute();

            return $query->fetchAll();
        }
        
        public function insertModule($code, $description){
            $query = $this->connection->prepare("
                INSERT INTO modules (
                  module_code,
                  module_description,
                  module_status
                ) VALUES (
                  :module_code,
                  :module_description,
                  :module_status
                )
            ");
                                                 
            $query->bindValue(":module_code", $code);
            $query->bindValue(":module_description", $description);
            $query->bindValue(":module_status", 0, PDO::PARAM_INT);
            $query->execute();

            return true;
        }
        
        public function changeStatus($code, $status){
            $query = $this->connection->prepare("
                UPDATE modules
                SET module_status = :module_status
                WHERE module_code = :module_code
            ");
                                                 
            $query->bindValue(":module_code", $code);
            $query->bindValue(":module_status", $status, PDO::PARAM_INT);

            $query->execute();

            return true;
        }
    }
